import { Injectable, EventEmitter, Output, Input } from '@angular/core';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/catch'
import 'rxjs/add/operator/map';
import { Scheduler } from 'rxjs/Scheduler';
import { async } from 'rxjs/scheduler/async';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/observeOn';

// var observable = Observable.create(function (observer) {
//   observer.next(1);
//   observer.next(2);
//   observer.next(3);
//   observer.complete();
// }).observeOn(async);

@Injectable()
export class CryptoConverterService {

  observable: any;
  constructor(public http: Http) {

  }

  convert(from, to) {
    return this.http.get('https://api.cryptonator.com/api/full/' + from + '-' + to)
    .map(response => response.json())
    .catch(this.handleError);
  }
  handleError(err) {
    return Observable.throw(err.json());
  }
}
