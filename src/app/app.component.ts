import { Component } from '@angular/core';
import { CryptoConverterService } from './cryptoConverter/crypto-converter.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  converts: any;
  currCode: any;
  informations: any;
  enterCurrencies: boolean = true;
  enterCrypto: boolean = false;
  title = 'Currency Forcasting';

  constructor(public currService: CryptoConverterService) { }

  currencyService(event, form: any) {
    console.log(event);
    this.aFun(form);
    this.enterCrypto = !this.enterCrypto;
    this.enterCurrencies = !this.enterCurrencies
  }

  aFun(form) {
    this.currCode = form.actCurrInput;
    this.currService.convert(form.cryptCrrInput, form.actCurrInput).subscribe(data => {
      this.converts = data;
      this.informations = data.ticker.markets;
      // this.enterCrypto = !this.enterCrypto;
      // this.enterCurrencies = !this.enterCurrencies
      setTimeout(() => {
        this.aFun(form);
      }, 5000)
    });
  }

  enterCryptoCurr() {
    this.enterCrypto = !this.enterCrypto;
    this.enterCurrencies = !this.enterCurrencies
  }
}
