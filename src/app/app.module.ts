import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { CryptoConverterService } from './cryptoConverter/crypto-converter.service';
import { Http, HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, HttpModule, HttpClientModule, FormsModule
  ],
  providers: [CryptoConverterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
